﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DartApp.Migrations
{
    public partial class AddedThrowMultipliers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FirstScoreMultiplier",
                table: "Throws",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SecondScoreMultiplier",
                table: "Throws",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ThirdScoreMultiplier",
                table: "Throws",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstScoreMultiplier",
                table: "Throws");

            migrationBuilder.DropColumn(
                name: "SecondScoreMultiplier",
                table: "Throws");

            migrationBuilder.DropColumn(
                name: "ThirdScoreMultiplier",
                table: "Throws");
        }
    }
}
