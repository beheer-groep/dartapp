﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DartApp.Migrations
{
    public partial class FixedTypos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SeccondScore",
                table: "Throws");

            migrationBuilder.DropColumn(
                name: "ThrirdScore",
                table: "Throws");

            migrationBuilder.AddColumn<int>(
                name: "SecondScore",
                table: "Throws",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ThirdScore",
                table: "Throws",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SecondScore",
                table: "Throws");

            migrationBuilder.DropColumn(
                name: "ThirdScore",
                table: "Throws");

            migrationBuilder.AddColumn<int>(
                name: "SeccondScore",
                table: "Throws",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ThrirdScore",
                table: "Throws",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
