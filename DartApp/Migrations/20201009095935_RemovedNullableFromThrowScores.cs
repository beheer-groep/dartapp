﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DartApp.Migrations
{
    public partial class RemovedNullableFromThrowScores : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Legs_Sets_SetId",
                table: "Legs");

            migrationBuilder.DropForeignKey(
                name: "FK_Sets_Games_GameId",
                table: "Sets");

            migrationBuilder.DropForeignKey(
                name: "FK_Throws_Legs_LegId",
                table: "Throws");

            migrationBuilder.AlterColumn<int>(
                name: "ThrirdScore",
                table: "Throws",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SeccondScore",
                table: "Throws",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FirstScore",
                table: "Throws",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Legs_Sets_SetId",
                table: "Legs",
                column: "SetId",
                principalTable: "Sets",
                principalColumn: "SetId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Sets_Games_GameId",
                table: "Sets",
                column: "GameId",
                principalTable: "Games",
                principalColumn: "GameId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Throws_Legs_LegId",
                table: "Throws",
                column: "LegId",
                principalTable: "Legs",
                principalColumn: "LegId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Legs_Sets_SetId",
                table: "Legs");

            migrationBuilder.DropForeignKey(
                name: "FK_Sets_Games_GameId",
                table: "Sets");

            migrationBuilder.DropForeignKey(
                name: "FK_Throws_Legs_LegId",
                table: "Throws");

            migrationBuilder.AlterColumn<int>(
                name: "ThrirdScore",
                table: "Throws",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "SeccondScore",
                table: "Throws",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "FirstScore",
                table: "Throws",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Legs_Sets_SetId",
                table: "Legs",
                column: "SetId",
                principalTable: "Sets",
                principalColumn: "SetId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sets_Games_GameId",
                table: "Sets",
                column: "GameId",
                principalTable: "Games",
                principalColumn: "GameId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Throws_Legs_LegId",
                table: "Throws",
                column: "LegId",
                principalTable: "Legs",
                principalColumn: "LegId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
