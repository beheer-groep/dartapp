﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace DartApp.Models.Entities
{
    public class Set
    {
        [JsonIgnore]
        public int SetId { get; set; }
        public int SequenceNumber { get; set; }

        [JsonIgnore]
        public int GameId { get; set; }
        [JsonIgnore]
        public Game Game { get; set; }

        [JsonIgnore]
        public int? WinnerId { get; set; }
        [JsonIgnore]
        public Player Winner { get; set; }

        private string _winnerName;
        [NotMapped]
        public string WinnerName
        {
            get => _winnerName ?? Winner?.Name;
            set => _winnerName = value;
        }

        public ICollection<Leg> Legs { get; set; }
    }
}
