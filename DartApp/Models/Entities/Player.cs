﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace DartApp.Models.Entities
{
    public class Player
    {
        public int PlayerId { get; set; }
        
        [Required]
        public string Name { get; set; }

        [JsonIgnore]
        public ICollection<Game> GamesPlayerOne { get; set; }
        [JsonIgnore]
        public ICollection<Game> GamesPlayerTwo { get; set; }
        [JsonIgnore]
        public ICollection<Game> GamesWon { get; set; }
        [JsonIgnore]
        public ICollection<Set> SetsWon { get; set; }
        [JsonIgnore]
        public ICollection<Leg> LegsWon { get; set; }
        [JsonIgnore]
        public ICollection<Throw> Throws { get; set; }
    }
}
