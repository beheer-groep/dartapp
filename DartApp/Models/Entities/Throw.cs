﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using DartApp.Controllers;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DartApp.Models.Entities
{
    public enum ThrowMultiplier { Single = 1, Double = 2, Triple = 3 }; // Geen bullseye, want bullseye is eigenlijk een double 25.

    public class Throw : IValidatableObject
    {
        [JsonIgnore]
        public int ThrowId { get; set; }
        public int SequenceNumber { get; set; }

        [JsonIgnore]
        public int LegId { get; set; }
        [JsonIgnore]
        public Leg Leg { get; set; }

        [JsonIgnore]
        public int PlayerId { get; set; }
        [JsonIgnore]
        public Player Player { get; set; }

        private string _playerName;
        [NotMapped]
        public string PlayerName
        {
            get => _playerName ?? Player?.Name;
            set => _playerName = value;
        }

        public int FirstScore { get; set; }
        public int SecondScore { get; set; }
        public int ThirdScore { get; set; }

        [Range(1,3)]
        public ThrowMultiplier FirstScoreMultiplier { get; set; }

        [Range(1, 3)]
        public ThrowMultiplier SecondScoreMultiplier { get; set; }

        [Range(1, 3)]
        public ThrowMultiplier ThirdScoreMultiplier { get; set; }

        [JsonIgnore]
        public int ActualFirstScore => FirstScore * (int)FirstScoreMultiplier;
        [JsonIgnore]
        public int ActualSecondScore => SecondScore * (int)SecondScoreMultiplier;
        [JsonIgnore]
        public int ActualThirdScore => ThirdScore * (int)ThirdScoreMultiplier;

        [JsonIgnore]
        public int Total => ActualFirstScore + ActualSecondScore + ActualThirdScore;

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var _context = (DartAppDatabaseContext)validationContext.GetService(typeof(DartAppDatabaseContext));

            var leftoverPoints = new LegsController(_context).GetLeftoverPointsForPlayer(LegId, PlayerId);

            if ((leftoverPoints - Total) < 0)
            {
                yield return new ValidationResult("The throw exceeds the leftover points for the player.",
                    new[] { nameof(Total) });
            }

            if (Total == leftoverPoints)
            {
                if (ActualThirdScore != 0)
                {
                    if (ThirdScoreMultiplier != ThrowMultiplier.Double)
                    {
                        yield return new ValidationResult("Must end on double",
                            new[] { nameof(ThirdScore) });
                    }
                }
                else if (ActualSecondScore != 0)
                {
                    if (SecondScoreMultiplier != ThrowMultiplier.Double)
                    {
                        yield return new ValidationResult("Must end on double",
                            new[] { nameof(SecondScore) });
                    }
                }
                else if (ActualFirstScore != 0)
                {
                    if (FirstScoreMultiplier != ThrowMultiplier.Double)
                    {
                        yield return new ValidationResult("Must end on double",
                            new[] { nameof(FirstScore) });
                    }
                }
            }
        }
    }
}
