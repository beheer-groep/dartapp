﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace DartApp.Models.Entities
{
    public class DartAppDatabaseContext : IdentityDbContext
    {
        public DbSet<Player> Players { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Set> Sets { get; set; }
        public DbSet<Leg> Legs { get; set; }
        public DbSet<Throw> Throws { get; set; }

        public DartAppDatabaseContext (DbContextOptions<DartAppDatabaseContext > options)
            : base(options)
        { 
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Game to Player relations
            builder.Entity<Game>()
                .HasOne(g => g.PlayerOne)
                .WithMany(p => p.GamesPlayerOne)
                .HasForeignKey(g => g.PlayerOneId)
                .OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Game>()
                .HasOne(g => g.PlayerTwo)
                .WithMany(p => p.GamesPlayerTwo)
                .HasForeignKey(g => g.PlayerTwoId)
                .OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Game>()
                .HasOne(g => g.Winner)
                .WithMany(p => p.GamesWon)
                .HasForeignKey(g => g.WinnerId)
                .OnDelete(DeleteBehavior.Restrict);

            // Set relations
            builder.Entity<Set>()
                .HasOne(s => s.Game)
                .WithMany(g => g.Sets)
                .HasForeignKey(s => s.GameId)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Set>()
                .HasOne(s => s.Winner)
                .WithMany(p => p.SetsWon)
                .HasForeignKey(s => s.WinnerId)
                .OnDelete(DeleteBehavior.Restrict);

            // Leg relations
            builder.Entity<Leg>()
                .HasOne(l => l.Set)
                .WithMany(s => s.Legs)
                .HasForeignKey(l => l.SetId)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Leg>()
                .HasOne(l => l.Winner)
                .WithMany(p => p.LegsWon)
                .HasForeignKey(l => l.WinnerId)
                .OnDelete(DeleteBehavior.Restrict);

            // Throw relations
            builder.Entity<Throw>()
                .HasOne(t => t.Leg)
                .WithMany(l => l.Throws)
                .HasForeignKey(t => t.LegId)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Throw>()
                .HasOne(t => t.Player)
                .WithMany(p => p.Throws)
                .HasForeignKey(t => t.PlayerId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
