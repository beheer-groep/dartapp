﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DartApp.Models.Entities
{
    public class Leg
    {
        [JsonIgnore]
        public int LegId { get; set; }
        public int SequenceNumber { get; set; }

        [JsonIgnore]
        public int SetId { get; set; }
        [JsonIgnore]
        public Set Set { get; set; }

        [JsonIgnore]
        public int? WinnerId { get; set; }
        [JsonIgnore]
        public Player Winner { get; set; }

        private string _winnerName;
        [NotMapped]
        public string WinnerName
        {
            get => _winnerName ?? Winner?.Name;
            set => _winnerName = value;
        }

        public ICollection<Throw> Throws { get; set; }
    }
}
