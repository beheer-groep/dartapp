﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace DartApp.Models.Entities
{
    public class Game : IValidatableObject
    {
        [JsonIgnore]
        public int GameId { get; set; }
        public DateTime GameDate { get; set; }

        [JsonIgnore]
        public int PlayerOneId { get; set; }
        [JsonIgnore]
        public int PlayerTwoId { get; set; }
        [JsonIgnore]
        public int? WinnerId { get; set; }

        public int LegAmount { get; set; }
        public int SetAmount { get; set; }
        public int StartingPoints { get; set; }

        [JsonIgnore]
        public Player PlayerOne { get; set; }
        [JsonIgnore]
        public Player PlayerTwo { get; set; }
        [JsonIgnore]
        public Player Winner { get; set; }

        private string _playerOneName;
        [NotMapped]
        public string PlayerOneName
        {
            get => _playerOneName ?? PlayerOne?.Name;
            set => _playerOneName = value;
        }

        private string _playerTwoName;
        [NotMapped]
        public string PlayerTwoName
        {
            get => _playerTwoName ?? PlayerTwo?.Name;
            set => _playerTwoName = value;
        }

        private string _winnerName;
        [NotMapped]
        public string WinnerName
        {
            get => _winnerName ?? Winner?.Name;
            set => _winnerName = value;
        }

        public ICollection<Set> Sets { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (SetAmount % 2 == 0)
                yield return new ValidationResult(
                    "Amount of sets must be an uneven number.",
                    new[] {nameof(SetAmount)});

            if (LegAmount % 2 == 0)
                yield return new ValidationResult(
                    "Amount of legs must be an uneven number.",
                    new[] {nameof(LegAmount)});

            var ctx = validationContext.GetRequiredService<DartAppDatabaseContext>();
            var storedGame = ctx.Games.AsNoTracking().FirstOrDefault(g => g.GameId == GameId);
            if (storedGame != null && storedGame.StartingPoints != StartingPoints &&
                ctx.Sets.Any(s => s.GameId == GameId))
                yield return new ValidationResult(
                    "Cannot change starting points when there are sets registered.",
                    new[] {nameof(StartingPoints)});
        }

        public string ToJson()
        {
            var json = JsonConvert.SerializeObject(this, new JsonSerializerSettings 
            { 
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize
            });
            return json;
        }

        public static Game FromJson(string json)
        {
            var game = JsonConvert.DeserializeObject<Game>(json);
            return game;
        }

        public class JSONSet
        {
            [JsonProperty("Legs")]
            public string legs { get; set; }
        }

        public class JSONLeg
        {
            [JsonProperty("Throws")]
            public string throws { get; set; }
        }



    }
}