﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DartApp.Models.ViewModels
{
    public class Gamestats
    {
        public int gameAverage { get; set; }
        public int playerOneAverage { get; set; }
        public int playerTwoAverage { get; set; }
        public int gameMaxThrowsCount { get; set; }
        public int playerOneMaxCount { get; set; }
        public int playerTwoMaxCount { get; set; }
        public List<Setstats> setstats { get; set; }

        public Gamestats()
        {
            setstats = new List<Setstats>();
        }
    }

    public class Setstats
    {
        public int avgPerSet { get; set; }
        public int maxPerSet { get; set; }

        public int playerOneAverage { get; set; }
        public int playerTwoAverage { get; set; }
        public List<Legstats> legstats { get; set; }

        public Setstats()
        {
            legstats = new List<Legstats>();
        }

        /*
        public Setstats(int average, int max, List<Legstats> legstat)
        {
            avgPerSet = average;
            maxPerSet = max;
            legstats = legstat;
        }
        */
    }

    public class Legstats
    {
        public int avgPerLeg { get; set; }
        public int maxPerLeg { get; set; }

        public int playerOneAverage { get; set; }
        public int playerTwoAverage { get; set; }

        public List<ThrowStats> throwstats { get; set; }

        public Legstats()
        {
            throwstats = new List<ThrowStats>();
        }

        /*
        public Legstats(int average, int max, int playerOne, int playerTwo)
        {
            avgPerLeg = average;
            maxPerLeg = max;
            playerOneAverage = playerOne;
            playerTwoAverage = playerTwo;
        }
        */
    }

    public class ThrowStats
    {

        public int playerOneAverage { get; set; }
        public int playerTwoAverage { get; set; }
        public ThrowStats(int playerOne, int playerTwo)
        {

            playerOneAverage = playerOne;
            playerTwoAverage = playerTwo;
        }
    }

}
