﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Sockets;
using System.Net;
namespace DartApp.Models.ViewModels

{

    public class Streamer
    {
        public JSONGame jsongame = null;
        public string stringData;
        public JSONGame Json { get => jsongame; set => jsongame = value; }

        //data classes
        public class JSONGame
        {
            [JsonProperty("StartingPoints")]
            public int startingPoints { get; set; }
            [JsonProperty("GameDate")]
            public string PlayerOne { get; set; }
            [JsonProperty("PlayerOne")]
            public string PlayerTwo { get; set; }
            [JsonProperty("PlayerTwo")]
            public string gameDate { get; set; }
            [JsonProperty("Sets")]
            public string sets { get; set; }
        }

        public class JSONSet
        {
            [JsonProperty("Legs")]
            public string legs { get; set; }
        }

        public class JSONLeg
        {
            [JsonProperty("Throws")]
            public string throws { get; set; }
        }
    }
}