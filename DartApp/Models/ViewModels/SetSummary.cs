namespace DartApp.Models.ViewModels
{
    public class SetSummary
    {
        public string PlayerOne { get; set; }
        public int PlayerOneSetTotal { get; set; }
        
        public string PlayerTwo { get; set; }
        public int PlayerTwoSetTotal { get; set; }
    }
}