﻿using DartApp.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DartApp.Models.ViewModels
{
    public class ThrowsIndexViewModel
    {
        public Leg Leg { get; set; }
        public List<Throw> Throws { get; set; }
    }
}
