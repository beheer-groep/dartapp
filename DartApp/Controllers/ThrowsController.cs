﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DartApp.Models.Entities;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace DartApp.Controllers
{
    [Authorize]
    public class ThrowsController : Controller
    {
        private readonly DartAppDatabaseContext _context;

        public ThrowsController(DartAppDatabaseContext context)
        {
            _context = context;
        }

        // GET: Throws
        public async Task<IActionResult> Index()
        {
            var throws = await _context.Throws
                .Include(t => t.Player)
                .Include(t => t.Leg)
                .OrderBy(t => t.LegId)
                .ThenBy(t => t.SequenceNumber)
                .ToListAsync();
            return View(throws);
        }

        // GET: Throws/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var @throw = await _context.Throws
                .Include(t => t.Leg)
                .Include(t => t.Player)
                .FirstOrDefaultAsync(m => m.ThrowId == id);
            if (@throw == null)
            {
                return NotFound();
            }

            return View(@throw);
        }

        // GET: Throws/Create
        public async Task<IActionResult> Create(int? legId)
        {
            if (legId == null)
                return NotFound();
            var leg = await _context.Legs
                .Include(l => l.Set)
                .FirstOrDefaultAsync(l => l.LegId == legId);
            var game = await _context.Games.Include(g => g.PlayerOne)
                .Include(g => g.PlayerTwo)
                .FirstOrDefaultAsync(g => g.GameId == leg.Set.GameId);
            List<Player> players = new List<Player>(new[] { game.PlayerOne, game.PlayerTwo });

            var nextPlayer = await GetNextPlayer(leg.LegId);
            
            ViewData["GameId"] = game.GameId;
            ViewData["LegId"] = leg.LegId;
            ViewData["Leg"] = leg;
            ViewData["PlayerId"] = new SelectList(players, "PlayerId", "Name", nextPlayer);
            ViewData["SequenceNumber"] = await GetNextSequence(leg.LegId);
            return View();
        }
        
        // POST: Throws/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GameId")] int? gameId, [Bind("ThrowId,SequenceNumber,Leg,LegId,PlayerId,FirstScore,SecondScore,ThirdScore,FirstScoreMultiplier,SecondScoreMultiplier,ThirdScoreMultiplier")] Throw @throw)
        {
            if (await LegFinished(@throw.LegId))
                return BadRequest("There is already a winner for this leg");
            
            if (ModelState.IsValid)
            {
                @throw.SequenceNumber = await GetNextSequence(@throw.LegId);
                _context.Add(@throw);
                await new LegsController(_context).CheckWinnerAsync(@throw.LegId);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Games", new { Id = gameId });
            }

            var nextPlayer = await GetNextPlayer(@throw.LegId);
            ViewData["GameId"] = gameId;
            ViewData["LegId"] = @throw.LegId;
            ViewData["PlayerId"] = new SelectList(_context.Players, "PlayerId", "Name", nextPlayer);
            return View(@throw);
        }

        // GET: Throws/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var @throw = await _context.Throws
                .Include(t => t.Leg)
                .ThenInclude(l => l.Set)
                .ThenInclude(s => s.Game)
                .FirstOrDefaultAsync(t => t.ThrowId == id);
            if (@throw == null)
                return NotFound();

            ViewData["GameId"] = @throw.Leg.Set.GameId;
            ViewData["PlayerId"] = new SelectList(_context.Players, "PlayerId", "Name", @throw.PlayerId);
            return View(@throw);
        }

        // POST: Throws/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("GameID")] int gameId, [Bind("ThrowId,SequenceNumber,LegId,PlayerId,FirstScore,SecondScore,ThirdScore,FirstScoreMultiplier,SecondScoreMultiplier,ThirdScoreMultiplier")] Throw @throw)
        {
            if (id != @throw.ThrowId)
                return NotFound();

            if (await LegFinished(@throw.LegId))
                return BadRequest("There is already a winner for this leg");

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(@throw);
                    await new LegsController(_context).CheckWinnerAsync(@throw.LegId);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ThrowExists(@throw.ThrowId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", "Games", new { Id = @throw.Leg.Set.GameId });
            }
            
            ViewData["GameId"] = gameId;
            ViewData["PlayerId"] = new SelectList(_context.Players, "PlayerId", "Name", @throw.PlayerId);
            return View(@throw);
        }

        // GET: Throws/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var @throw = await _context.Throws
                .Include(t => t.Leg)
                .Include(t => t.Player)
                .FirstOrDefaultAsync(m => m.ThrowId == id);
            if (@throw == null)
                return NotFound();

            return View(@throw);
        }

        // POST: Throws/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var @throw = await _context.Throws.FindAsync(id);
            
            if (await LegFinished(@throw.LegId))
                return BadRequest("There is already a winner for this leg");
            
            _context.Throws.Remove(@throw);
            await new LegsController(_context).CheckWinnerAsync(@throw.LegId);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "Games", new { Id = @throw.Leg.Set.GameId });
        }

        private bool ThrowExists(int id)
        {
            return _context.Throws.Any(e => e.ThrowId == id);
        }

        private async Task<int> GetNextSequence(int legId)
        {
            var latestThrow = await _context.Throws
                .Where(t => t.LegId == legId)
                .OrderByDescending(t => t.SequenceNumber)
                .FirstOrDefaultAsync();
            if (latestThrow == null)
                return 1;

            return latestThrow.SequenceNumber + 1;
        }
        
        private async Task<bool> LegFinished(int legId)
        {
            var leg = await _context.Legs.FirstAsync(l => l.LegId == legId);
            return leg.WinnerId.HasValue;
        }
        
        private async Task<int?> GetNextPlayer(int legId)
        {
            var leg = await _context.Legs
                .Include(l => l.Set).ThenInclude(s => s.Game)
                .Include(l => l.Throws)
                .Where(l => l.LegId == legId)
                .FirstOrDefaultAsync();
            if (leg == null)
                throw new ArgumentException("Leg not found");

            var lastThrow = leg.Throws
                .OrderBy(t => t.SequenceNumber) 
                .LastOrDefault();
            if (lastThrow == null)
                return null;

            var game = leg.Set.Game;
            return game.PlayerOneId == lastThrow.PlayerId ? game.PlayerTwoId : game.PlayerOneId;
        }
    }
}
