﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DartApp.Models.Entities;
using Microsoft.AspNetCore.Authorization;

namespace DartApp.Controllers
{
    [Authorize]
    public class LegsController : Controller
    {
        private readonly DartAppDatabaseContext _context;

        public LegsController(DartAppDatabaseContext context)
        {
            _context = context;
        }

        public async Task CheckWinnerAsync(int id)
        {
            // Variables
            var leg = _context.Legs.Include(l => l.Throws).ToList().Find(l => l.LegId == id);
            _context.Update(leg);
            var set = await _context.Sets.FindAsync(leg.SetId);
            var game = await _context.Games.FindAsync(set.GameId);

            // Check if either player won
            var playerOneId = game.PlayerOneId;
            SetWinnerIfPlayerWonLeg(playerOneId, leg);
            var playerTwoId = game.PlayerTwoId;
            SetWinnerIfPlayerWonLeg(playerTwoId, leg);

            await _context.SaveChangesAsync();

            new SetsController(_context).CheckWinner(leg.SetId);
        }
        
        private void SetWinnerIfPlayerWonLeg(int playerId, Leg leg)
        {
            // Get total score for player and check if he won
            var throwsForPlayer = leg.Throws.Where(t => t.PlayerId == playerId);
            var totalScoreForPlayer = GetTotalScoreForPlayerInThrow(throwsForPlayer);
            // Check if the input player has won the game, and assign him as winner if needed
            if (totalScoreForPlayer == leg.Set.Game.StartingPoints)
            {
                leg.WinnerId = playerId;
            }
        }

        public static int GetTotalScoreForPlayerInThrow(IEnumerable<Throw> throwsForPlayer)
        {
            int totalScore = 0;

            foreach (Throw t in throwsForPlayer)
            {
                totalScore += t.Total;
            }

            return totalScore;
        }

        // GET: Legs
        public async Task<IActionResult> Index()
        {
            var dartAppDatabaseContext = _context.Legs.Include(l => l.Set).Include(l => l.Winner);
            return View(await dartAppDatabaseContext.ToListAsync());
        }

        // GET: Legs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leg = await _context.Legs
                .Include(l => l.Set)
                .Include(l => l.Winner)
                .FirstOrDefaultAsync(m => m.LegId == id);
            if (leg == null)
            {
                return NotFound();
            }

            return View(leg);
        }

        // GET: Legs/Create
        public async Task<IActionResult> Create(int setId)
        {
            var @set = await _context.Sets.FirstOrDefaultAsync(s => s.SetId == setId);
            if (@set == null)
                return NotFound();

            ViewData["GameId"] = @set.GameId;
            ViewData["SetId"] = @set.SetId;
            ViewData["SequenceNumber"] = await GetNextSequence(setId);
            ViewData["WinnerId"] = new SelectList(_context.Players, "PlayerId", "Name");
            return View();
        }

        // POST: Legs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("LegId,SequenceNumber,SetId,WinnerId")] Leg leg)
        {
            if (ModelState.IsValid)
            {
                var canCreate = await SetHasMaxAmountOfLegs(leg.SetId);
                if (!canCreate)
                    return BadRequest();

                leg.SequenceNumber = await GetNextSequence(leg.SetId);
                _context.Add(leg);
                new SetsController(_context).CheckWinner(leg.SetId);
                await _context.SaveChangesAsync();
                
                return RedirectToAction("Details", "Games", new { Id = leg.Set.GameId });
            }

            ViewData["SetId"] = leg.SetId;
            ViewData["WinnerId"] = new SelectList(_context.Players, "PlayerId", "Name", leg.WinnerId);
            return View(leg);
        }

        // GET: Legs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();

            var leg = await _context.Legs
                .Include(l => l.Set)
                .FirstOrDefaultAsync(l => l.LegId == id);
            if (leg == null)
                return NotFound();
            
            ViewData["SetId"] = new SelectList(_context.Sets, "SetId", "SetId", leg.SetId);
            ViewData["WinnerId"] = new SelectList(_context.Players, "PlayerId", "Name", leg.WinnerId);
            return View(leg);
        }

        // POST: Legs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("LegId,SequenceNumber,SetId,WinnerId")] Leg leg)
        {
            if (id != leg.LegId)
            {
                return NotFound();
            }

            if (await HasWinner(id))
                return BadRequest("Leg already has a winner");

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(leg);
                    new SetsController(_context).CheckWinner(leg.SetId);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LegExists(leg.LegId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", "Games", new { Id = leg.Set.GameId });
            }
            ViewData["SetId"] = new SelectList(_context.Sets, "SetId", "SetId", leg.SetId);
            ViewData["WinnerId"] = new SelectList(_context.Players, "PlayerId", "Name", leg.WinnerId);
            return View(leg);
        }

        // GET: Legs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leg = await _context.Legs
                .Include(l => l.Set)
                .Include(l => l.Winner)
                .FirstOrDefaultAsync(m => m.LegId == id);
            if (leg == null)
            {
                return NotFound();
            }

            return View(leg);
        }

        // POST: Legs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var leg = await _context.Legs.FindAsync(id);
            
            if (leg.WinnerId.HasValue)
                return BadRequest("Leg already has a winner");

            _context.Legs.Remove(leg);
            new SetsController(_context).CheckWinner(leg.SetId);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "Games", new { Id = leg.Set.GameId });
        }

        private bool LegExists(int id)
        {
            return _context.Legs.Any(e => e.LegId == id);
        }

        private async Task<int> GetNextSequence(int setId)
        {
            var latestLeg = await _context.Legs
                .Where(l => l.SetId == setId)
                .OrderByDescending(l => l.SequenceNumber)
                .FirstOrDefaultAsync();
            if (latestLeg == null)
                return 1;

            return latestLeg.SequenceNumber + 1;
        }

        private async Task<bool> SetHasMaxAmountOfLegs(int setId)
        {
            var set = await _context.Sets
                .Include(s => s.Game)
                .Include(s => s.Legs)
                .FirstAsync(s => s.SetId == setId);

            return set.Game.LegAmount > set.Legs.Count;
        }

        public static int GetRemainingPointsForPlayerAfterThrow(IEnumerable<Throw> throws, int startingPoints)
        {
            var points = startingPoints;

            foreach (Throw t in throws)
            {
                points -= t.Total;
            }
            return points;
        }

        public int GetLeftoverPointsForPlayer(int legId, int playerId)
        {
            var leg = _context.Legs
                .Include(l => l.Set)
                .ThenInclude(s => s.Game)
                .Include(l => l.Throws)
                .AsNoTracking()
                .FirstOrDefault(l => l.LegId == legId);
            var throws = leg.Throws.Where(t => t.PlayerId == playerId);

            var points = leg.Set.Game.StartingPoints;

            var Remain = GetRemainingPointsForPlayerAfterThrow(throws, points);

            return Remain;
        }

        private async Task<bool> HasWinner(int legId)
        {
            var leg = await _context.Legs.FirstAsync(l => l.LegId == legId);
            return leg.WinnerId.HasValue;
        }
    }
}
