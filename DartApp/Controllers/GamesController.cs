﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DartApp.Models.Entities;
using DartApp.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Text;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;

namespace DartApp.Controllers
{
    [Authorize]
    public class GamesController : Controller
    {
        private readonly DartAppDatabaseContext _context;

        public GamesController(DartAppDatabaseContext context)
        {
            _context = context;
        }

        public void CheckWinner(int id)
        {
            var game = _context.Games.Include(g => g.Sets).ToList().Find(g => g.GameId == id);
            _context.Update(game);
            var sets = game.Sets;

            int winsPlayerOne = 0;
            int winsPlayerTwo = 0;

            foreach (Set set in sets)
            {
                var winnerId = set.WinnerId;

                if (winnerId == game.PlayerOneId)
                {
                    winsPlayerOne++;
                }
                else if (winnerId == game.PlayerTwoId)
                {
                    winsPlayerTwo++;
                }
                else
                {
                    continue;
                }

                int setWinsForGameWin = game.SetAmount / 2 + 1;
                if (setWinsForGameWin == winsPlayerOne)
                {
                    game.WinnerId = game.PlayerOneId;
                }
                else if (setWinsForGameWin == winsPlayerTwo)
                {
                    game.WinnerId = game.PlayerTwoId;
                }
                else
                {
                    game.WinnerId = null;
                }
            }
        }

        // GET: Games
        public async Task<IActionResult> Index()
        {
            var dartAppDatabaseContext = _context.Games
                .Include(g => g.PlayerOne)
                .Include(g => g.PlayerTwo)
                .Include(g => g.Winner)
                .OrderByDescending(g => g.GameDate);
            return View(await dartAppDatabaseContext.ToListAsync());
        }

        // GET: Games/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var game = await _context.Games
                .Include(g => g.PlayerOne)
                .Include(g => g.PlayerTwo)
                .Include(g => g.Winner)
                .Include(g => g.Sets)
                .ThenInclude(s => s.Legs)
                .ThenInclude(l => l.Throws)
                .FirstOrDefaultAsync(m => m.GameId == id);
            if (game == null)
                return NotFound();

            ViewData["Summary"] = await GetGameSummary(game.GameId);
            ViewData["Stats"] = GetStats(game);
            return View(game);

        }

        //POST: Games/Import
        public IActionResult Import()
        {
            return View();
        }

        //import
        [HttpPost]
        public async Task<IActionResult> Import(IFormFile file)
        {
            if (file != null)
            {
                if (file.Length < 4096 && file.FileName.EndsWith(".JSON"))
                {
                    ViewBag.Message = "Bestand geupload! Even geduld...";
                    Debug.WriteLine(file.FileName);

                    var result = new StringBuilder();
                    using (var reader = new StreamReader(file.OpenReadStream()))
                    {
                        while (reader.Peek() >= 0)
                            result.AppendLine(await reader.ReadLineAsync());
                    }

                    try
                    {
                        var newGameId = await Importer(result.ToString());
                        return RedirectToAction("Details", new { Id = newGameId });
                    }
                    catch
                    {
                        ViewBag.Message = "Er ging iets fout bij het importeren";
                    }
                }
                else if (!file.FileName.EndsWith(".json"))
                {
                    ViewBag.Message = "Foutief bestand geupload. Upload een .json bestand.";
                }
                else if (file.Length > 4096)
                {
                    ViewBag.Message = "Bestand te groot. Upload een kleiner bestand.";
                }
            }
            else
            {
                ViewBag.Message = "Geen bestand geupload, selecteer een bestand.";
            }

            return View();
        }

        private async Task<int> Importer(string stringData)
        {
            var game = Game.FromJson(stringData);
            var playerOneId = await GetPlayerIdOrCreateNew(game.PlayerOneName);
            var playerTwoId = await GetPlayerIdOrCreateNew(game.PlayerTwoName);
            var playerNameMap = new Dictionary<string, int>()
            {
                { game.PlayerOneName, playerOneId },
                { game.PlayerTwoName, playerTwoId }
            };

            var newGame = new Game()
            {
                GameDate = game.GameDate,
                StartingPoints = game.StartingPoints,
                LegAmount = game.LegAmount,
                SetAmount = game.SetAmount,
                PlayerOneId = playerOneId,
                PlayerTwoId = playerTwoId,
                Sets = new List<Set>()
            };
            if (game.WinnerName != null)
                newGame.WinnerId = playerNameMap[game.WinnerName];

            foreach (var set in game.Sets)
            {
                var newSet = new Set()
                {
                    Legs = new List<Leg>(),
                    SequenceNumber = set.SequenceNumber
                };
                if (set.WinnerName != null)
                    newSet.WinnerId = playerNameMap[set.WinnerName];
                
                foreach (var leg in set.Legs)
                {
                    var newLeg = new Leg()
                    {
                        SequenceNumber = leg.SequenceNumber,
                        Throws = new List<Throw>()
                    };
                    if (leg.WinnerName != null)
                        newLeg.WinnerId = playerNameMap[leg.WinnerName];

                    foreach (var @throw in leg.Throws)
                    {
                        var newThrow = new Throw()
                        {
                            PlayerId = playerNameMap[@throw.PlayerName],
                            FirstScore = @throw.FirstScore,
                            FirstScoreMultiplier = @throw.FirstScoreMultiplier,
                            SecondScore = @throw.SecondScore,
                            SecondScoreMultiplier = @throw.SecondScoreMultiplier,
                            ThirdScore = @throw.ThirdScore,
                            ThirdScoreMultiplier = @throw.ThirdScoreMultiplier,
                            SequenceNumber = @throw.SequenceNumber
                        };

                        newLeg.Throws.Add(newThrow);
                    }

                    newSet.Legs.Add(newLeg);
                }

                newGame.Sets.Add(newSet);
            }

            _context.Add(newGame);
            await _context.SaveChangesAsync();
            return newGame.GameId;
        }


        public async Task<int> GetPlayerIdOrCreateNew(string playerName)
        {
            var player = await _context.Players
                .Where(p => p.Name == playerName)
                .FirstOrDefaultAsync();
            if (player != null)
                return player.PlayerId;

            var newPlayer = new Player() { Name = playerName };
            _context.Add(newPlayer);
            await _context.SaveChangesAsync();
            return newPlayer.PlayerId;
        }

        // GET: Games/Create
        public IActionResult Create()
        {
            ViewData["PlayerOneId"] = new SelectList(_context.Players, "PlayerId", "Name");
            ViewData["PlayerTwoId"] = new SelectList(_context.Players, "PlayerId", "Name");
            ViewData["WinnerId"] = new SelectList(_context.Players, "PlayerId", "Name");
            return View();
        }

        // POST: Games/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind("GameId,GameDate,PlayerOneId,PlayerTwoId,WinnerId,LegAmount,SetAmount,StartingPoints")]
            Game game)
        {
            if (ModelState.IsValid)
            {
                _context.Add(game);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["PlayerOneId"] = new SelectList(_context.Players, "PlayerId", "Name", game.PlayerOneId);
            ViewData["PlayerTwoId"] = new SelectList(_context.Players, "PlayerId", "Name", game.PlayerTwoId);
            ViewData["WinnerId"] = new SelectList(_context.Players, "PlayerId", "Name", game.WinnerId);
            return View(game);
        }

        // GET: Games/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var game = await _context.Games.FindAsync(id);
            if (game == null)
            {
                return NotFound();
            }

            ViewData["PlayerOneId"] = new SelectList(_context.Players, "PlayerId", "Name", game.PlayerOneId);
            ViewData["PlayerTwoId"] = new SelectList(_context.Players, "PlayerId", "Name", game.PlayerTwoId);
            ViewData["WinnerId"] = new SelectList(_context.Players, "PlayerId", "Name", game.WinnerId);
            return View(game);
        }

        // POST: Games/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            [Bind("GameId,GameDate,PlayerOneId,PlayerTwoId,WinnerId,LegAmount,SetAmount,StartingPoints")]
            Game game)
        {
            if (id != game.GameId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(game);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GameExists(game.GameId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
            }

            ViewData["PlayerOneId"] = new SelectList(_context.Players, "PlayerId", "Name", game.PlayerOneId);
            ViewData["PlayerTwoId"] = new SelectList(_context.Players, "PlayerId", "Name", game.PlayerTwoId);
            ViewData["WinnerId"] = new SelectList(_context.Players, "PlayerId", "Name", game.WinnerId);
            return View(game);
        }

        // GET: Games/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return NotFound();

            var game = await _context.Games
                .Include(g => g.PlayerOne)
                .Include(g => g.PlayerTwo)
                .Include(g => g.Winner)
                .FirstOrDefaultAsync(m => m.GameId == id);
            if (game == null)
                return NotFound();

            return View(game);
        }

        // POST: Games/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var game = await _context.Games.FindAsync(id);
            _context.Games.Remove(game);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public int AveragePer(int throws, int total)
        {
            int avgPer;
            if (throws == 0 || total == 0)
            {
                avgPer = 0;
            }
            else
            {
                avgPer = total / throws;
            }
            return avgPer;
        }

        private bool GameExists(int id)
        {
            return _context.Games.Any(e => e.GameId == id);
        }

        public async Task<List<SetSummary>> GetGameSummary(int gameId)
        {
            var game = await _context.Games
                .Include(g => g.Sets)
                .ThenInclude(s => s.Legs)
                .FirstOrDefaultAsync(g => g.GameId == gameId);
            if (game == null)
                throw new ArgumentException("Game not found");

            return game.Sets.Select(set => new SetSummary()
            {
                PlayerOne = game.PlayerOne.Name,
                PlayerOneSetTotal = set.Legs.Count(l => l.WinnerId == game.PlayerOneId),

                PlayerTwo = game.PlayerTwo.Name,
                PlayerTwoSetTotal = set.Legs.Count(l => l.WinnerId == game.PlayerTwoId)
            }).ToList();
        }

        public async Task<IActionResult> Export(int? id)
        {
            if (id == null)
                return NotFound();

            var game = await _context.Games
                .Include(g => g.PlayerOne)
                .Include(g => g.PlayerTwo)
                .Include(g => g.Winner)
                .Include(g => g.Sets)
                .ThenInclude(s => s.Legs)
                .ThenInclude(l => l.Throws)
                .FirstOrDefaultAsync(m => m.GameId == id);
            var exportData = game.ToJson();

            return File(System.Text.Encoding.UTF8.GetBytes(exportData),
                "text/JSON",
                string.Format("Game at {0}.JSON", game.GameDate));
        }


        public Gamestats GetStats(Game game)
        {
            //game statistics
            int gameTotalScore = 0; //total score per game
            int gameThrowsCount = 0; //throws per game
            int gameMaxThrowsCount = 0; //max scores per game
            int playerOneTotalScore = 0; //total score player one
            int playerTwoTotalScore = 0; //total score player two
            int playerOneThrowsCount = 0; //total throws player one
            int playerTwoThrowsCount = 0; //total throws player two
            int playerOneMaxCount = 0; //max score throws player one
            int playerTwoMaxCount = 0; //max score throws player two
                                       //stats

            var stats = new Gamestats();

            var sets = game.Sets;
            foreach (var @set in sets)
            {
                int maxPerSet = 0; //max score per set
                int totalPerSet = 0;
                int throwsPerSet = 0;
                int throwsSetPlayerOne = 0;
                int throwsSetPlayerTwo = 0;
                int totalSetPlayerOne = 0;
                int totalSetPlayerTwo = 0;

                var setStats = new Setstats();

                foreach (var leg in set.Legs)
                {
                    gameThrowsCount = gameThrowsCount + leg.Throws.Count;
                    int maxPerLeg = 0; //max score per leg
                    int totalPerLeg = 0;
                    int throwsPerLeg = 0;
                    int throwsLegPlayerOne = 0;
                    int throwsLegPlayerTwo = 0;
                    int totalLegPlayerOne = 0;
                    int totalLegPlayerTwo = 0;

                    var legStats = new Legstats();


                    foreach (var @throw in leg.Throws)
                    {
                        gameTotalScore = gameTotalScore + @throw.Total;

                        totalPerSet = totalPerSet + @throw.Total;
                        throwsPerSet++;

                        totalPerLeg = totalPerLeg + @throw.Total;
                        throwsPerLeg++;

                        int throwAvgPlayerOne = 0;
                        int throwAvgPlayerTwo = 0;

                        if (@throw.PlayerId == game.PlayerOneId)
                        {
                            playerOneTotalScore = playerOneTotalScore + @throw.Total; //total score player one
                            playerOneThrowsCount++;
                            throwsSetPlayerOne++;
                            totalSetPlayerOne = totalSetPlayerOne + @throw.Total;
                            throwsLegPlayerOne++;
                            totalLegPlayerOne = totalLegPlayerOne + @throw.Total;
                            throwAvgPlayerOne = AveragePer(3, @throw.Total);

                        }
                        if (@throw.PlayerId == game.PlayerTwoId)
                        {
                            playerTwoTotalScore = playerTwoTotalScore + @throw.Total; //total score player two
                            playerTwoThrowsCount++;
                            throwsSetPlayerTwo++;
                            totalSetPlayerTwo = totalSetPlayerTwo + @throw.Total;
                            throwsLegPlayerTwo++;
                            totalLegPlayerTwo = totalLegPlayerTwo + @throw.Total;
                            throwAvgPlayerTwo = AveragePer(3, @throw.Total);
                        }

                        var throwStats = new ThrowStats(throwAvgPlayerOne, throwAvgPlayerTwo);

                        //max scores
                        if (@throw.Total == 180)
                        {
                            gameMaxThrowsCount++; //max scores per game
                            maxPerSet++; //max score per set
                            maxPerLeg++; //max score per leg
                            if (@throw.PlayerId == game.PlayerOneId)
                            {
                                playerOneMaxCount++; //max score throws player one
                            }
                            if (@throw.PlayerId == game.PlayerTwoId)
                            {
                                playerTwoMaxCount++; //max score throws player two
                            }
                        }

                        legStats.throwstats.Add(throwStats);
                    }
                    int avgPerLeg = AveragePer(throwsPerLeg, totalPerLeg); //average per leg 
                    int legAvgPlayerOne = AveragePer(throwsLegPlayerOne, totalLegPlayerOne);
                    int legAvgPlayerTwo = AveragePer(throwsLegPlayerTwo, totalLegPlayerTwo);

                    legStats.avgPerLeg = avgPerLeg;
                    legStats.maxPerLeg = maxPerLeg;
                    legStats.playerOneAverage = legAvgPlayerOne;
                    legStats.playerTwoAverage = legAvgPlayerTwo;

                    setStats.legstats.Add(legStats);
                }
                int avgPerSet = AveragePer(throwsPerSet, totalPerSet); //average per set

                //var setStats = new Setstats(avgPerSet,maxPerSet, legStats);

                setStats.maxPerSet = maxPerSet;
                setStats.avgPerSet = avgPerSet;
                setStats.playerOneAverage = AveragePer(throwsSetPlayerOne, totalSetPlayerOne);
                setStats.playerTwoAverage = AveragePer(throwsSetPlayerTwo, totalSetPlayerTwo);

                stats.setstats.Add(setStats);
            }
            //game stats
            stats.gameMaxThrowsCount = gameMaxThrowsCount; //max scores per game
            stats.gameAverage = AveragePer(gameThrowsCount, gameTotalScore); //average score per game
            stats.playerOneAverage = AveragePer(playerOneThrowsCount, playerOneTotalScore); //average player one
            stats.playerTwoAverage = AveragePer(playerTwoThrowsCount, playerTwoTotalScore); //average player two 
            stats.playerOneMaxCount = playerOneMaxCount; //max score throws player one
            stats.playerTwoMaxCount = playerTwoMaxCount; //max score throws player two

            return stats;



        }
    }
}