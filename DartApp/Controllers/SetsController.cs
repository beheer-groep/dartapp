﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DartApp.Models.Entities;
using Microsoft.AspNetCore.Authorization;

namespace DartApp.Controllers
{
    [Authorize]
    public class SetsController : Controller
    {
        private readonly DartAppDatabaseContext _context;

        public SetsController(DartAppDatabaseContext context)
        {
            _context = context;
        }

        public void CheckWinner(int id)
        {
            var set = _context.Sets.Include(s => s.Legs).Include(s => s.Game).ToList().Find(s => s.SetId == id);
            _context.Update(set);
            var legs = set.Legs;

            int winsPlayerOne = 0;
            int winsPlayerTwo = 0;

            foreach (Leg leg in legs)
            {
                var winnerId = leg.WinnerId;

                if (winnerId == set.Game.PlayerOneId)
                {
                    winsPlayerOne++;
                }
                else if (winnerId == set.Game.PlayerTwoId)
                {
                    winsPlayerTwo++;
                }
                else
                {
                    continue;
                }

                int legWinsForSetWin = set.Game.LegAmount / 2 + 1;
                if (legWinsForSetWin == winsPlayerOne)
                {
                    set.WinnerId = set.Game.PlayerOneId;
                }
                else if (legWinsForSetWin == winsPlayerTwo)
                {
                    set.WinnerId = set.Game.PlayerTwoId;
                }
                else
                {
                    set.WinnerId = null;
                }
            }
            new GamesController(_context).CheckWinner(set.GameId);
        }

        // GET: Sets
        public async Task<IActionResult> Index()
        {
            var dartAppDatabaseContext = _context.Sets.Include(@set => @set.Game).Include(@set => @set.Winner);
            return View(await dartAppDatabaseContext.ToListAsync());
        }

        // GET: Sets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @set = await _context.Sets
                .Include(@set => @set.Game)
                .Include(@set => @set.Winner)
                .FirstOrDefaultAsync(m => m.SetId == id);
            if (@set == null)
            {
                return NotFound();
            }

            return View(@set);
        }

        // GET: Sets/Create
        public async Task<IActionResult> Create([FromQuery] int gameId)
        {
            ViewData["GameId"] = new SelectList(_context.Games, "GameId", "GameId");
            ViewData["WinnerId"] = new SelectList(_context.Players, "PlayerId", "Name");
            ViewData["SequenceNumber"] = await GetNextSequence(gameId);
            return View();
        }

        // POST: Sets/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SetId,SequenceNumber,GameId,WinnerId")] Set @set)
        {
            var game = await _context.Games
                .Include(g => g.Sets)
                .FirstOrDefaultAsync(g => g.GameId == @set.GameId);
            if (game == null)
                return NotFound();

            if (GameHasMaxSets(game.SetAmount, game.Sets.Count) == false)
            {
                ViewData["GameID"] = new SelectList(_context.Games, "GameId", "GameId", @set.GameId);
                ViewData["WinnerID"] = new SelectList(_context.Players, "PlayerId", "Name", @set.WinnerId);

                if (ModelState.IsValid)
                {
                    @set.SequenceNumber = await GetNextSequence(game.GameId);
                    _context.Add(@set);
                    new GamesController(_context).CheckWinner(@set.GameId);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Details", "Games", new { Id = game.GameId });
                }

                return View(@set);
            }
            else
            {
                //return with error message
                ViewData["Message"] = "May not create more sets, maximum already reached";
                return NotFound();
            }
        }

        // GET: Sets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @set = await _context.Sets.FindAsync(id);
            if (@set == null)
            {
                return NotFound();
            }
            ViewData["GameId"] = new SelectList(_context.Games, "GameId", "GameId", @set.GameId);
            ViewData["WinnerId"] = new SelectList(_context.Players, "PlayerId", "Name", @set.WinnerId);
            return View(@set);
        }

        // POST: Sets/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SetId,SequenceNumber,GameId,WinnerId")] Set @set)
        {
            if (id != @set.SetId)
            {
                return NotFound();
            }

            if (await HasWinner(id))
                return BadRequest("Set already has a winner");

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(@set);
                    new GamesController(_context).CheckWinner(@set.GameId);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SetExists(@set.SetId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", "Games", new { Id = @set.GameId });
            }
            ViewData["GameId"] = new SelectList(_context.Games, "GameId", "GameId", @set.GameId);
            ViewData["WinnerId"] = new SelectList(_context.Players, "PlayerId", "Name", @set.WinnerId);
            return View(@set);
        }

        // GET: Sets/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @set = await _context.Sets
                .Include(@set => @set.Game)
                .Include(@set => @set.Winner)
                .FirstOrDefaultAsync(m => m.SetId == id);
            if (@set == null)
            {
                return NotFound();
            }

            return View(@set);
        }

        // POST: Sets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var @set = await _context.Sets.FindAsync(id);

            if (@set.WinnerId.HasValue)
                return BadRequest("Set already has a winner");
            
            _context.Sets.Remove(@set);
            new GamesController(_context).CheckWinner(set.GameId);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "Games", new { Id = @set.GameId });
        }

        private bool SetExists(int id)
        {
            return _context.Sets.Any(e => e.SetId == id);
        }

        //check if max sets is hit
        public static bool GameHasMaxSets(int maxSets, int totalCreatedSets)
        {
            return maxSets <= totalCreatedSets;
        }

        private async Task<int> GetNextSequence(int gameId)
        {
            var latestSet = await _context.Sets
                .Where(s => s.GameId == gameId)
                .OrderByDescending(s => s.SequenceNumber)
                .FirstOrDefaultAsync();

            if (latestSet == null)
                return 1;

            return latestSet.SequenceNumber + 1;    
        }

        private async Task<bool> HasWinner(int setId)
        {
            var set = await _context.Sets.FirstAsync(s => s.SetId == setId);
            return set.WinnerId.HasValue;
        }
    }
}
