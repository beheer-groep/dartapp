using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DartApp.Models.Entities;
using DartAppTests.Controllers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DartAppTests.Entities
{
    [TestClass]
    public class GameValidationTest : ApplicationContextTestClass
    {
        [DataTestMethod]
        [DataRow(3, 1, true)] // Sets and legs ok
        [DataRow(2, 1, false)] // Sets invalid and legs ok
        [DataRow(3, 2, false)] // Sets ok and legs invalid
        public void GameValidationTests(int sets, int legs, bool valid)
        {
            // Arrange
            var game = new Game()
            {
                GameDate = new DateTime(2020, 1, 1, 12, 0, 0),
                StartingPoints = 301,
                SetAmount = sets,
                LegAmount = legs
            };
            var validationContext = PrepareValidationContext(game);

            // Act
            var validationResult = game.Validate(validationContext).ToList();
            
            // Assert
            Assert.AreEqual(!valid, validationResult.Any());
        }

        [TestMethod]
        public void GameValidationShouldFailWhenStartingPointsChangeWhenThereIsASetRegistered()
        {
            // Arrange
            var game = new Game()
            {
                PlayerOneId = ApplicationContextSeeder.ObjectIds["playerA"],
                PlayerTwoId = ApplicationContextSeeder.ObjectIds["playerB"],
                GameDate = new DateTime(2020, 1, 1, 12, 0, 0),
                LegAmount = 1,
                SetAmount = 1,
                StartingPoints = 301,
                Sets = new List<Set>()
                {
                    new Set() {SequenceNumber = 1}
                }
            };
            context.Add(game);
            context.SaveChanges();
            var validationContext = PrepareValidationContext(game);
            
            // Act
            game.StartingPoints = 501;
            var validationResult = game.Validate(validationContext);
            
            // Assert
            Assert.IsTrue(validationResult.Any());
        }
        
        [TestMethod]
        public void GameValidationShouldSucceedWhenStartingPointsChangeWhenThereAreNoSetsRegistered()
        {
            // Arrange
            var game = new Game()
            {
                PlayerOneId = ApplicationContextSeeder.ObjectIds["playerA"],
                PlayerTwoId = ApplicationContextSeeder.ObjectIds["playerB"],
                GameDate = new DateTime(2020, 1, 1, 12, 0, 0),
                LegAmount = 1,
                SetAmount = 1,
                StartingPoints = 301
            };
            context.Add(game);
            context.SaveChanges();
            var validationContext = PrepareValidationContext(game);
            
            // Act
            game.StartingPoints = 501;
            var validationResult = game.Validate(validationContext);
            
            // Assert
            Assert.IsFalse(validationResult.Any());
        }
        
        private ValidationContext PrepareValidationContext(Game game)
        {
            var services = new ServiceCollection()
                .AddSingleton(context)
                .BuildServiceProvider();
            var validationContext = new ValidationContext(game, services, null);
            return validationContext;
        }
    }
}