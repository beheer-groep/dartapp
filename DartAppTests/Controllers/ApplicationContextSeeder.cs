﻿using System;
using System.Collections.Generic;
using DartApp.Models.Entities;

namespace DartAppTests.Controllers
{
    public static class ApplicationContextSeeder
    {
        public static Dictionary<string, int> ObjectIds;
        
        public static void SeedContext(this DartAppDatabaseContext context)
        {
            ObjectIds = new Dictionary<string, int>();
            
            //seed players
            var playerA = new Player() { Name = "Player A" };
            var playerB = new Player() { Name = "Player B" };
            context.AddRange(playerA, playerB);
            context.SaveChanges();
            ObjectIds.Add("playerA", playerA.PlayerId);
            ObjectIds.Add("playerB", playerB.PlayerId);

            //seed game
            var game = new Game()
            {
                GameDate = new DateTime(2020, 01, 01, 12, 0, 0),
                LegAmount = 3,
                SetAmount = 3,
                StartingPoints = 301,
                PlayerOne = playerA,
                PlayerTwo = playerB
            };
            var gameForCheckWinner = new Game()
            {
                StartingPoints = 301,
                SetAmount = 3,
                PlayerOne = playerA,
                PlayerTwo = playerB
            };
            context.AddRange(gameForCheckWinner, game);
            context.SaveChanges();

            //seed set
            var @set = new Set()
            {
                GameId = 1,
                WinnerId = 1,
                SequenceNumber = 1
            };
            var setForCheckWinnerTest = new Set()
            {
                GameId = 1
            };
            var setOneForGameCheckWinnerTest = new Set()
            {
                Game = gameForCheckWinner,
                WinnerId = 1
            };
            var setTwoForGameCheckWinnerTest = new Set()
            {
                Game = gameForCheckWinner,
                WinnerId = 1
            };
            context.AddRange(@set, setForCheckWinnerTest, setOneForGameCheckWinnerTest, setTwoForGameCheckWinnerTest);
            context.SaveChanges();

            //seed leg
            var leg = new Leg()
            {
                SetId = 1,
                SequenceNumber = 1
            };
            context.Add(leg);
            context.SaveChanges();
            
            //seed throws
            var @throw = new Throw()
            {
                Leg = leg,
                Player = playerA,
                FirstScore = 10,
                SecondScore = 5,
                SequenceNumber = 1
            };

            var throw2 = new Throw()
            {
                Leg = leg,
                Player = playerB,
                FirstScore = 301,
                FirstScoreMultiplier = ThrowMultiplier.Single,
                SequenceNumber = 2
            };
            context.AddRange(@throw, throw2);
            context.SaveChanges();

            SeedLegAmountLimitTests(context);
            SeedSetForSummary(context);
        }

        private static void SeedLegAmountLimitTests(DartAppDatabaseContext context)
        {
            var game = new Game()
            {
                GameDate = new DateTime(2020, 1, 1, 12, 0, 0),
                LegAmount = 1,
                SetAmount = 3,
                StartingPoints = 301,
                PlayerOneId = ObjectIds["playerA"],
                PlayerTwoId = ObjectIds["playerB"]
            };
            context.Add(game);
            context.SaveChanges();

            // Set with legs
            var setWithLegs = new Set()
            {
                Game = game,
                SequenceNumber = 1
            };
            context.Add(setWithLegs);
            context.SaveChanges();
            ObjectIds.Add("setWithLegs", setWithLegs.SetId);

            var leg = new Leg()
            {
                Set = setWithLegs,
                SequenceNumber = 1
            };
            context.Add(leg);
            context.SaveChanges();
            
            // Set without legs
            var setWithoutLegs = new Set()
            {
                Game = game,
                SequenceNumber = 2,
            };
            context.Add(setWithoutLegs);
            context.SaveChanges();
            ObjectIds.Add("setWithoutLegs", setWithoutLegs.SetId);
        }

        private static void SeedSetForSummary(DartAppDatabaseContext ctx)
        {
            var game = new Game()
            {
                GameDate = new DateTime(2020, 01, 01, 12, 00, 00),
                LegAmount = 3,
                SetAmount = 1,
                PlayerOneId = ObjectIds["playerA"],
                PlayerTwoId = ObjectIds["playerB"],
                StartingPoints = 301,
                Sets = new List<Set>()
                {
                    new Set()
                    {
                        SequenceNumber = 1,
                        Legs = new List<Leg>()
                        {
                            new Leg() { SequenceNumber = 1, WinnerId = ObjectIds["playerA"] },
                            new Leg() { SequenceNumber = 2, WinnerId = ObjectIds["playerA"] },
                            new Leg() { SequenceNumber = 3, WinnerId = ObjectIds["playerB"] },
                        }
                    }
                }
            };

            ctx.Add(game);
            ctx.SaveChanges();
            ObjectIds.Add("gameForSummary", game.GameId);
        }
    }
}