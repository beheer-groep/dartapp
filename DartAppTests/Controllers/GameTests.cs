using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DartApp.Controllers;
using DartApp.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DartAppTests.Controllers
{
    [TestClass]
    public class GameTests : ApplicationContextTestClass
    {
        private GamesController gamesController;

        [TestInitialize]
        public void Initialize()
        {
            gamesController = new GamesController(context);
        }

        #region Index Tests
        [TestMethod]
        public async Task GameIndexShouldReturnAllGamesInDatabase()
        {
            // Arange
            var games = await context.Games.OrderByDescending(g => g.GameDate).ToListAsync();

            // Act
            var results = await gamesController.Index();

            // Assert
            var view = results as ViewResult;
            Assert.IsNotNull(view);
            var model = view.Model as List<Game>;
            Assert.IsNotNull(model);
            CollectionAssert.AreEqual(games, model);
        }
        #endregion

        #region Detail Tests
        [TestMethod]
        public async Task GameDetailsShould404WhenNoGameIdIsGiven()
        {
            // Act
            var request = await gamesController.Details(null);

            // Assert
            Assert.IsInstanceOfType(request, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task GameDetailsShould404WhenIdIsGivenThatDoesNotExists()
        {
            // Act
            var request = await gamesController.Details(9000);

            // Assert
            Assert.IsInstanceOfType(request, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task GameDetailsShouldReturnDetailsWhenGameIsFound()
        {
            // Arrange
            var game = await context.Games.FirstAsync(g => g.GameId == 1);

            // Act
            var result = await gamesController.Details(1);

            // Assert
            var viewResult = result as ViewResult;
            Assert.IsNotNull(viewResult);
            var model = viewResult.Model as Game;
            Assert.IsNotNull(model);
            Assert.AreEqual(game.GameId, model.GameId);
        }
        #endregion

        #region Create Tests
        [TestMethod]
        public async Task GameCreateShouldCreateANewGameAndRedirectsToIndex()
        {
            // Arrange
            var playerA = await context.Players.FirstAsync(p => p.PlayerId == 1);
            var playerB = await context.Players.FirstAsync(p => p.PlayerId == 2);

            var game = new Game()
            {
                GameDate = new DateTime(2020, 1, 1, 12, 0, 0),
                PlayerOne = playerA,
                PlayerTwo = playerB,
                SetAmount = 3,
                LegAmount = 3,
                StartingPoints = 301,
                Winner = playerB
            };

            // Act
            var result = await gamesController.Create(game);

            // Assert
            var view = result as RedirectToActionResult;
            Assert.IsNotNull(view);
            Assert.AreEqual("Index", view.ActionName);

            var storedGame = await context.Games.OrderBy(g => g.GameId).LastAsync();
            Assert.AreEqual(game.GameDate, storedGame.GameDate);
            Assert.AreEqual(game.PlayerOneId, storedGame.PlayerOneId);
            Assert.AreEqual(game.PlayerTwoId, storedGame.PlayerTwoId);
            Assert.AreEqual(game.SetAmount, storedGame.SetAmount);
            Assert.AreEqual(game.LegAmount, storedGame.LegAmount);
            Assert.AreEqual(game.StartingPoints, storedGame.StartingPoints);
            Assert.AreEqual(game.WinnerId, storedGame.WinnerId);
        }
        #endregion
        
        #region Delete Tests
        [TestMethod]
        public async Task GameDeleteShouldReturn404WhenNoIdIsProvided()
        {
            // Act
            var result = await gamesController.Delete(null);

            // Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task GameDeleteShouldReturn404WhenGameNotFound()
        {
            // Act
            var result = await gamesController.Delete(9000);

            // Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task GameDeleteShouldReturnConfirmPageWhenFound()
        {
            // Arrang
            var game = await context.Games.FirstAsync(g => g.GameId == 1);

            // Act
            var result = await gamesController.Delete(1);

            // Assert
            var view = result as ViewResult;
            Assert.IsNotNull(view);
            var model = view.Model as Game;
            Assert.IsNotNull(model);
            Assert.AreEqual(game.GameId, model.GameId);
        }

        [TestMethod]
        public void GameDeleteConfirmedShouldThrowWhenGameNotFound()
        {
            // Act => Assert
            Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await gamesController.DeleteConfirmed(2));
        }

        [TestMethod]
        public async Task GameDeleteConfirmShouldDeleteWhenGameIsFoundAndRedirect()
        {
            // Act
            var result = await gamesController.DeleteConfirmed(1);

            // Assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            var isGameDelete = context.Games.Any(g => g.GameId == 1) == false;
            Assert.IsTrue(isGameDelete);
        }
        #endregion

        #region CheckWinner Tests
        [TestMethod]
        public async Task TestCheckWinner_InputIsSetsWithWinners_OutputIsCorrectSetWinner()
        {
            //Arrange
            var game = await context.Games.FindAsync(2);
            var playerA = context.Players.Where(p => p.Name == "Player A").First();

            //Act
            new GamesController(context).CheckWinner(game.GameId);

            //Assert
            Assert.AreEqual(game.WinnerId, playerA.PlayerId);
        }
        #endregion

        #region CheckAVG Tests
        [TestMethod]
        public void AvgPer()
        {
            //Arrange
            int avg = 12 / 4;
            //Act
            int avgper = new GamesController(context).AveragePer(4,12);
            //Assert
            Assert.AreEqual(avg, avgper);
        }

        [TestMethod]
        public async Task NullTest()
        {
            var game = await context.Games.FirstAsync(g => g.GameId == 1);
            Assert.IsNotNull(game);
        }

        [TestMethod]
        public async Task TestStats()
        {
            // Arrange
            //var game = await context.Games.FirstAsync(g => g.GameId == 1);
            var game = await context.Games
                .Include(g => g.Sets)
                .ThenInclude(s => s.Legs)
                .FirstOrDefaultAsync(g => g.GameId == 1);
            if (game == null)
                throw new ArgumentException("Game not found");

            //Act
            var checkstats = gamesController.GetStats(game);

            //game statistics
            int gameTotalScore = 0; //total score per game
            int gameThrowsCount = 0; //throws per game
            int gameMaxThrowsCount = 0; //max scores per game
            int playerOneTotalScore = 0; //total score player one
            int playerTwoTotalScore = 0; //total score player two
            int playerOneThrowsCount = 0; //total throws player one
            int playerTwoThrowsCount = 0; //total throws player two
            int playerOneMaxCount = 0; //max score throws player one
            int playerTwoMaxCount = 0; //max score throws player two

            //stats
            var sets = game.Sets;
            foreach (var @set in sets)
            {
                foreach (var leg in set.Legs)
                {
                    foreach (var @throw in leg.Throws)
                    {
                        gameTotalScore = gameTotalScore + @throw.Total;
                        gameThrowsCount++;

                        if (@throw.PlayerId == game.PlayerOneId)
                        {
                            playerOneTotalScore = playerOneTotalScore + @throw.Total; //total score player one
                            playerOneThrowsCount++;

                        }
                        if (@throw.PlayerId == game.PlayerTwoId)
                        {
                            playerTwoTotalScore = playerTwoTotalScore + @throw.Total; //total score player two
                            playerTwoThrowsCount++;
                        }

                        //max scores
                        if (@throw.Total == 180)
                        {
                            gameMaxThrowsCount++; //max scores per game
                            if (@throw.PlayerId == game.PlayerOneId)
                            {
                                playerOneMaxCount++; //max score throws player one
                            }
                            if (@throw.PlayerId == game.PlayerTwoId)
                            {
                                playerTwoMaxCount++; //max score throws player two
                            }
                        }
                    }
                }
            }

            int gameAvg = new GamesController(context).AveragePer(gameThrowsCount, gameTotalScore);
            int playerOneAvg = new GamesController(context).AveragePer(playerOneThrowsCount, playerOneTotalScore);
            int playerTwoAvg = new GamesController(context).AveragePer(playerTwoThrowsCount, playerTwoTotalScore);
            //Assert
            Assert.AreEqual(checkstats.playerOneAverage, playerOneAvg);
            Assert.AreEqual(checkstats.playerTwoAverage, playerTwoAvg);
            Assert.AreEqual(checkstats.gameAverage, gameAvg);
            Assert.AreEqual(checkstats.gameMaxThrowsCount, gameMaxThrowsCount);
            Assert.AreEqual(checkstats.playerOneMaxCount, playerOneMaxCount);
            Assert.AreEqual(checkstats.playerTwoMaxCount, playerTwoMaxCount);
        }
        #endregion

        [TestMethod]
        public async Task TestGameSummary()
        {
            // Arrange
            var game = await context.Games
                .FirstAsync(g => g.GameId == (int)ApplicationContextSeeder.ObjectIds["gameForSummary"]);
            
            // Act
            var result = await gamesController.GetGameSummary(game.GameId);
            
            // Assert
            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Player A", result.First().PlayerOne);
            Assert.AreEqual("Player B", result.First().PlayerTwo);
            Assert.AreEqual(2, result.First().PlayerOneSetTotal);
            Assert.AreEqual(1, result.First().PlayerTwoSetTotal);
        }
    }
}
