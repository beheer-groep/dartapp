﻿using System;
using System.Data.Common;
using DartApp.Models.Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace DartAppTests.Controllers
{
    public abstract class ApplicationContextTestClass : IDisposable
    {
        private readonly DbConnection _connection;
        protected readonly DartAppDatabaseContext context;

        public ApplicationContextTestClass()
        {
            _connection = CreateInMemoryDatabase();

            var contextOptions = new DbContextOptionsBuilder<DartAppDatabaseContext>()
                .UseSqlite(_connection)
                .EnableSensitiveDataLogging(true)
                .Options;
            context = new DartAppDatabaseContext(contextOptions);
            context.Database.EnsureCreated();
            context.SeedContext();
        }

        private static DbConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("Filename=:memory:");
            connection.Open();

            return connection;
        }

        public void Dispose()
        {
            context.Dispose();
            _connection.Dispose();
        }
    }
}