using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DartApp.Controllers;
using DartApp.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DartAppTests.Controllers
{
    [TestClass]
    public class SetTests : ApplicationContextTestClass
    {
        private SetsController setsController;
        [TestInitialize]
        public void Initialize()
        {
            setsController = new SetsController(context);
        }

        [TestMethod]
        public async Task SetsIndexShouldReturnAllSetsInDatabase()
        {
            // Arange
            var sets = await context.Sets.ToListAsync();

            // Act
            var results = await setsController.Index();

            // Assert
            var view = results as ViewResult;
            Assert.IsNotNull(view);
            var model = view.Model as List<Set>;
            Assert.IsNotNull(model);
            CollectionAssert.AreEqual(sets, model);
        }


        [TestMethod]
        public async Task SetDetailsShould404WhenNoSetIdIsGiven()
        {
            // Act
            var request = await setsController.Details(null);

            // Assert
            Assert.IsInstanceOfType(request, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task SetDetailsShould404WhenIdIsGivenThatDoesNotExists()
        {
            // Act
            var request = await setsController.Details(9000);

            // Assert
            Assert.IsInstanceOfType(request, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task SetDetailsShouldReturnDetailsWhenSetIsFound()
        {
            // Arrange
            var sets = await context.Sets.FirstAsync(g => g.SetId == 1);

            // Act
            var result = await setsController.Details(1);

            // Assert
            var viewResult = result as ViewResult;
            Assert.IsNotNull(viewResult);
            var model = viewResult.Model as Set;
            Assert.IsNotNull(model);
            Assert.AreEqual(sets.SetId, model.SetId);
        }

        [TestMethod]
        public async Task EditSetShouldReturnBadRequestWhenSetHasAWinner()
        {
            // Assert
            var set = await context.Sets.FirstAsync(s => s.SetId == 1);
            set.WinnerId = ApplicationContextSeeder.ObjectIds["playerA"];
            await context.SaveChangesAsync();

            // Act
            var result = await setsController.Edit(set.SetId, set);

            // Assert
            var view = result as BadRequestObjectResult;
            Assert.IsNotNull(view);
        }

        [TestMethod]
        public async Task TestEditSets_ShouldReturnSetsWhenSetsIsFound()
        {
            // Arrange
            var @set = await context.Sets.FirstAsync(g => g.SetId == 1);

            // Act
            var result = await setsController.Edit(1); //edit seed

            // Assert
            var viewResult = result as ViewResult;
            Assert.IsNotNull(viewResult);
            var model = viewResult.Model as Set;
            Assert.IsNotNull(model);
            Assert.AreEqual(@set.SetId, model.SetId);
        }

        [TestMethod]
        public async Task SetEditPostShouldEditModelInDatabaseWithNewValues()
        {
            // Arrange
            var playerA = await context.Players.FirstAsync(p => p.PlayerId == 1);
            var playerB = await context.Players.FirstAsync(p => p.PlayerId == 2);
            var @set = await context.Sets.FirstAsync(g => g.SetId == 1);
            @set.GameId = 1;
            @set.SequenceNumber = 2;

            // Act
            var response = await setsController.Edit(1, @set);

            // Assert
            var redirectResult = response as RedirectToActionResult;
            Assert.IsNotNull(redirectResult);
            //stored
            var storedSet = await context.Sets.FirstAsync(g => g.SetId == 1);
            //test
            Assert.AreEqual(@set.GameId, storedSet.GameId);
            Assert.AreEqual(@set.SequenceNumber, storedSet.SequenceNumber);
        }


        [TestMethod]
        public async Task SetDeleteShouldReturn404WhenNoIdIsProvided()
        {
            // Act
            var result = await setsController.Delete(null);

            // Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task SetDeleteShouldReturn404WhenSetNotFound()
        {
            // Act
            var result = await setsController.Delete(9000);

            // Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task SetDeleteShouldReturnConfirmPageWhenFound()
        {
            // Arrang
            var sets = await context.Sets.FirstAsync(g => g.SetId == 1);

            // Act
            var result = await setsController.Delete(1);

            // Assert
            var view = result as ViewResult;
            Assert.IsNotNull(view);
            var model = view.Model as Set;
            Assert.IsNotNull(model);
            Assert.AreEqual(sets.SetId, model.SetId);
        }

        [TestMethod]
        public void SetDeleteConfirmedShouldThrowWhenSetNotFound()
        {
            // Act => Assert
            Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await setsController.DeleteConfirmed(1));
        }

        [TestMethod]
        public async Task SetDeleteConfirmShouldDeleteWhenSetIsFoundAndRedirect()
        {
            // Act
            var result = await setsController.DeleteConfirmed(1);

            // Assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            var isSetDelete = context.Sets.Any(g => g.SetId == 1) == false;
            Assert.IsTrue(isSetDelete);
        }

        [TestMethod]
        public async Task DeleteConfirmSetShouldReturnBadRequestWhenSetHasAWinner()
        {
            // Assert
            var set = await context.Sets.FirstAsync(s => s.SetId == 1);
            set.WinnerId = ApplicationContextSeeder.ObjectIds["playerA"];
            await context.SaveChangesAsync();

            // Act
            var result = await setsController.DeleteConfirmed(set.SetId);

            // Assert
            var view = result as BadRequestObjectResult;
            Assert.IsNotNull(view);
        }

        [TestMethod]
        public async Task SetCreateShouldCreateANewSetAndRedirectsToGameDetails()
        {
            // Arrange
            var playerA = await context.Players.FirstAsync(p => p.PlayerId == 1);
            var playerB = await context.Players.FirstAsync(p => p.PlayerId == 2);

            var @sets = new Set()
            {
                WinnerId = playerB.PlayerId,
                GameId = 1,
            };

            // Act
            var result = await setsController.Create(sets);

            // Assert
            var view = result as RedirectToActionResult;
            Assert.IsNotNull(view);
            Assert.AreEqual("Details", view.ActionName);
            Assert.AreEqual("Games", view.ControllerName);

            var storedSet = await context.Sets.OrderBy(g => g.SetId).LastAsync();
            Assert.AreEqual(@sets.GameId, storedSet.GameId);
            Assert.AreEqual(@sets.WinnerId, storedSet.WinnerId);
            
            // One set is already in test model with sequence 1, this one should be 2.
            Assert.AreEqual(2, storedSet.SequenceNumber);
        }

        //test true or false max sets
        [TestMethod]
        public void MaxSetsBoolIfFalse()
        {
            int SetAmount = 1;
            int CreatedAmount = 1;

            // Act
            var response = SetsController.GameHasMaxSets(SetAmount, CreatedAmount);

            //test
            Assert.AreEqual(true, response);
        }

        #region CheckWinner Tests
        [TestMethod]
        public async Task TestCheckWinner_InputIsLegsWithWinners_OutputIsCorrectSetWinner()
        {
            //Arrange
            var set = await context.Sets.FindAsync(2);
            var playerA = context.Players.Where(p => p.Name == "Player A").First();

            //Act
            new SetsController(context).CheckWinner(set.SetId);

            //Assert
            Assert.AreEqual(set.WinnerId, playerA.PlayerId);
        }
        #endregion
    }
}
