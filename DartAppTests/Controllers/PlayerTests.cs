using DartApp.Controllers;
using DartApp.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections;
using Microsoft.EntityFrameworkCore.Query;

namespace DartAppTests.Controllers
{
    [TestClass]
    public class PlayerTests : ApplicationContextTestClass
    {
        private PlayersController playerController;

        [TestInitialize]
        public void Initialize()
        {
            playerController = new PlayersController(context);
        }

        [TestMethod]
        public async Task PlayerDetailsShouldReturnAllGamesForPlayer()
        {
            // Arange
            var playerId = 1;
            var games = context.Games.Where(g => g.PlayerOneId == playerId || g.PlayerTwoId == playerId).ToList();

            // Act
            var results = await playerController.Details(playerId);

            // Assert
            var view = results as ViewResult;
            Assert.IsNotNull(view);
            var model = ((IQueryable<Game>)view.ViewData["Games"]).ToList();
            Assert.IsNotNull(model);
            CollectionAssert.AreEqual(games, model);
        }
    }
}
