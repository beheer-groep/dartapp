using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DartApp.Controllers;
using DartApp.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DartAppTests.Controllers
{
    [TestClass]
    public class LegTests : ApplicationContextTestClass
    {
        private LegsController legsController;

        [TestInitialize]
        public void Initialize()
        {
            legsController = new LegsController(context);
        }

        #region Edit Tests
        [TestMethod]
        public async Task LegEditShouldGiveBadRequestWhenTheLegAlreadyHasAWinner()
        {
            // Arrange
            var leg = await context.Legs.FirstAsync(g => g.LegId == 1);
            leg.WinnerId = ApplicationContextSeeder.ObjectIds["playerA"];
            await context.SaveChangesAsync();

            // Act
            var result = await legsController.Edit(leg.LegId, leg);

            // Assert
            var view = result as BadRequestObjectResult;
            Assert.IsNotNull(view);
        }

        [TestMethod]
        public async Task TestEditLegs_ShouldReturnSetsWhenLegsIsFound()
        {
            // Arrange
            var leg = await context.Legs.FirstAsync(g => g.LegId == 1);

            // Act
            var result = await legsController.Edit(1); //edit leg

            // Assert
            var viewResult = result as ViewResult;
            Assert.IsNotNull(viewResult);
            var model = viewResult.Model as Leg;
            Assert.IsNotNull(model);
            Assert.AreEqual(leg.LegId, model.LegId);
        }

        [TestMethod]
        public async Task LegEditPostShouldEditModelInDatabaseWithNewValues()
        {
            // Arrange
            var playerA = await context.Players.FirstAsync(p => p.PlayerId == 1);
            var playerB = await context.Players.FirstAsync(p => p.PlayerId == 2);
            var leg = await context.Legs.FirstAsync(g => g.LegId == 1);
            leg.SetId = 1;
            leg.SequenceNumber = 2;

            // Act
            var response = await legsController.Edit(1, leg);

            // Assert
            var redirectResult = response as RedirectToActionResult;
            Assert.IsNotNull(redirectResult);
            //stored
            var storedLeg = await context.Legs.FirstAsync(g => g.LegId == 1);
            //test
            Assert.AreEqual(leg.SetId, storedLeg.SetId);
            Assert.AreEqual(leg.SequenceNumber, storedLeg.SequenceNumber);
        }
        #endregion

        #region Create Tests
        [TestMethod]
        public async Task LegCreateShouldCreateANewLegAndRedirectsToGameDetails()
        {
            // Arrange
            var playerA = await context.Players.FirstAsync(p => p.PlayerId == 1);
            var playerB = await context.Players.FirstAsync(p => p.PlayerId == 2);

            var leg = new Leg()
            {
                SetId = 1,
                Winner = playerA
            };

            // Act
            var result = await legsController.Create(leg);

            // Assert
            var view = result as RedirectToActionResult;
            Assert.IsNotNull(view);
            Assert.AreEqual("Details", view.ActionName);
            Assert.AreEqual("Games", view.ControllerName);

            var storedLeg = await context.Legs.OrderBy(g => g.LegId).LastAsync();
            Assert.AreEqual(leg.WinnerId, storedLeg.WinnerId);
            Assert.AreEqual(leg.SetId, storedLeg.SetId);
            
            // One leg is already created with sequence number 1, this one should be 2.
            Assert.AreEqual(2, storedLeg.SequenceNumber);
        }

        [TestMethod]
        public async Task LegCreateShouldSucceedWhenNotOnLegLimit()
        {
            // Arrange
            var leg = new Leg()
            {
                SequenceNumber = 1,
                SetId = ApplicationContextSeeder.ObjectIds["setWithoutLegs"]
            };

            // Act
            var result = await legsController.Create(leg);

            // Assert
            var view = result as RedirectToActionResult;
            Assert.IsNotNull(view);
        }

        [TestMethod]
        public async Task LegCreateShouldGiveBadRequestWhenOnLegLimit()
        {
            // Arrange
            var leg = new Leg()
            {
                SequenceNumber = 1,
                SetId = ApplicationContextSeeder.ObjectIds["setWithLegs"]
            };

            // Act
            var result = await legsController.Create(leg);

            // Assert
            var view = result as BadRequestResult;
            Assert.IsNotNull(view);
        }
        #endregion

        #region Delete Tests
        [TestMethod]
        public async Task LegDeleteConfirmShouldGiveBadRequestWhenLegAlreadyHasAWinner()
        {
            // Arrange
            var leg = await context.Legs.FirstAsync(g => g.LegId == 1);
            leg.WinnerId = ApplicationContextSeeder.ObjectIds["playerA"];
            await context.SaveChangesAsync();

            // Act
            var result = await legsController.DeleteConfirmed(leg.LegId);

            // Assert
            var view = result as BadRequestObjectResult;
            Assert.IsNotNull(view);
        }

        [TestMethod]
        public async Task LegDeleteShouldReturn404WhenNoIdIsProvided()
        {
            // Act
            var result = await legsController.Delete(null);

            // Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task LegDeleteShouldReturn404WhenLegNotFound()
        {
            // Act
            var result = await legsController.Delete(9000);

            // Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task LegDeleteShouldReturnConfirmPageWhenFound()
        {
            // Arrang
            var leg = await context.Legs.FirstAsync(g => g.LegId == 1);

            // Act
            var result = await legsController.Delete(1);

            // Assert
            var view = result as ViewResult;
            Assert.IsNotNull(view);
            var model = view.Model as Leg;
            Assert.IsNotNull(model);
            Assert.AreEqual(leg.LegId, model.LegId);
        }

        [TestMethod]
        public void LegDeleteConfirmedShouldThrowWhenLegNotFound()
        {
            // Act => Assert
            Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await legsController.DeleteConfirmed(2));
        }

        [TestMethod]
        public async Task LegDeleteConfirmShouldDeleteWhenLegIsFoundAndRedirect()
        {
            // Act
            var result = await legsController.DeleteConfirmed(1);

            // Assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            var isLegDelete = context.Legs.Any(l => l.LegId == 1) == false;
            Assert.IsTrue(isLegDelete);
        }
        #endregion

        #region Details Tests
        [TestMethod]
        public async Task LegDetailsShould404WhenNoLegIdIsGiven()
        {
            // Act
            var request = await legsController.Details(null);

            // Assert
            Assert.IsInstanceOfType(request, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task LegDetailsShould404WhenIdIsGivenThatDoesNotExists()
        {
            // Act
            var request = await legsController.Details(9000);

            // Assert
            Assert.IsInstanceOfType(request, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task LegDetailsShouldReturnDetailsWhenLegIsFound()
        {
            // Arrange
            var Leg = await context.Legs.FirstAsync(g => g.LegId == 1);

            // Act
            var result = await legsController.Details(1);

            // Assert
            var viewResult = result as ViewResult;
            Assert.IsNotNull(viewResult);
            var model = viewResult.Model as Leg;
            Assert.IsNotNull(model);
            Assert.AreEqual(Leg.LegId, model.LegId);
        }
        #endregion

        #region Index Tests
        [TestMethod]
        public async Task LegIndexShouldReturnAllLegsInDatabase()
        {
            // Arange
            var legs = await context.Legs.ToListAsync();

            // Act
            var results = await legsController.Index();

            // Assert
            var view = results as ViewResult;
            Assert.IsNotNull(view);
            var model = view.Model as List<Leg>;
            Assert.IsNotNull(model);
            CollectionAssert.AreEqual(legs, model);
        }
        #endregion

        #region CheckWinnerAsync Tests
        [TestMethod]
        public async Task TestCheckWinnerAsync_InputIsLegWithPlayer301Points_OutputIsCorrectWinner()
        {
            //Arrange
            var leg = context.Legs.ToList().Find(l => l.SetId == 1);
            var playerB = context.Players.Where(p => p.Name == "Player B").First();

            //Act
            await new LegsController(context).CheckWinnerAsync(leg.LegId);

            //Assert
            Assert.AreEqual(leg.WinnerId, playerB.PlayerId);
        }
        #endregion

        #region GetLeftoverPointsForPlayer Tests
        [TestMethod]
        public void TestGetLeftoverPointsForPlayer_InputIsLegAnPlayer_OutputIsCorrectPoints()
        {
            //Arrange
            var leg = context.Legs.ToList().Find(l => l.SetId == 1);
            var playerB = context.Players.Where(p => p.Name == "Player B").First();

            //Act
            var result = new LegsController(context).GetLeftoverPointsForPlayer(leg.LegId, playerB.PlayerId);

            //Assert
            Assert.AreEqual(0, result);
        }
        #endregion
    }
}
