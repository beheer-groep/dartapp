using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DartApp.Controllers;
using DartApp.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DartAppTests.Controllers
{
    [TestClass]
    public class ThrowTests : ApplicationContextTestClass
    {
        private ThrowsController throwsController;

        [TestInitialize]
        public void Initialize()
        {
            throwsController = new ThrowsController(context);
        }

        #region Index
        [TestMethod]
        public async Task ThrowIndexShouldReturnAllThrowsInDatabase()
        {
            // Arange
            var throws = await context.Throws
                .OrderBy(t => t.LegId)
                .ThenBy(t => t.SequenceNumber)
                .ToListAsync();

            // Act
            var results = await throwsController.Index();

            // Assert
            var view = results as ViewResult;
            Assert.IsNotNull(view);
            var model = view.Model as List<Throw>;
            Assert.IsNotNull(model);
            CollectionAssert.AreEqual(throws, model);
        }
        #endregion

        #region Create
        [TestMethod]
        public async Task ThrowCreateShouldCreateANewThrowAndRedirectsToGameDetails()
        {
            // Arrange
            var player = await context.Players.FirstAsync();
            var leg = await context.Legs
                .Include(l => l.Set)
                .FirstAsync();
            var @throw = new Throw()
            {
                LegId = leg.LegId,
                Player = player,
                FirstScore = 10,
                SecondScore = 15
            };

            // Act
            var result = await throwsController.Create(leg.Set.GameId, @throw);

            // Assert
            var view = result as RedirectToActionResult;
            Assert.IsNotNull(view);
            Assert.AreEqual("Details", view.ActionName);
            Assert.AreEqual("Games", view.ControllerName);

            var storedThrow = await context.Throws.OrderBy(t => t.ThrowId).LastAsync();
            Assert.AreEqual(@throw.LegId, storedThrow.LegId);
            Assert.AreEqual(@throw.PlayerId, storedThrow.PlayerId);
            Assert.AreEqual(@throw.FirstScore, storedThrow.FirstScore);
            Assert.AreEqual(@throw.SecondScore, storedThrow.SecondScore);
            Assert.AreEqual(@throw.ThirdScore, 0);
            
            // Two throws are already seeded, sequence must be 3
            Assert.AreEqual(3, storedThrow.SequenceNumber);
        }
        #endregion

        #region Read
        [TestMethod]
        public async Task ThrowDetailsShould404WhenNoIdIsGiven()
        {
            // Act
            var request = await throwsController.Details(null);

            // Assert
            Assert.IsInstanceOfType(request, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task ThrowDetailsShould404WhenIdIsGivenThatDoesNotExists()
        {
            // Act
            var request = await throwsController.Details(3);

            // Assert
            Assert.IsInstanceOfType(request, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task ThrowDetailsShouldReturnDetailsWhenTaskIsFound()
        {
            // Arrange
            var task = await context.Games.FirstAsync(g => g.GameId == 1);

            // Act
            var result = await throwsController.Details(1);

            // Assert
            var viewResult = result as ViewResult;
            Assert.IsNotNull(viewResult);
            var model = viewResult.Model as Throw;
            Assert.IsNotNull(model);
            Assert.AreEqual(task.GameId, model.ThrowId);
        }
        #endregion

        #region Update
        [TestMethod]
        public async Task ThrowEditShouldReturnSetsWhenSetsIsFound()
        {
            // Arrange
            var @throw = await context.Throws.FirstAsync(g => g.ThrowId == 1);

            // Act
            var result = await throwsController.Edit(1); //edit seed

            // Assert
            var viewResult = result as ViewResult;
            Assert.IsNotNull(viewResult);
            var model = viewResult.Model as Throw;
            Assert.IsNotNull(model);
            Assert.AreEqual(@throw.ThrowId, model.ThrowId);
        }

        [TestMethod]
        public async Task ThrowEditPostShouldEditModelInDatabaseWithNewValues()
        {
            // Arrange
            var player = await context.Players.FirstAsync(p => p.PlayerId == 2);
            var @throw = await context.Throws.FirstAsync(g => g.ThrowId == 1);
            @throw.FirstScore = 3;
            @throw.SecondScore = 2;
            @throw.ThirdScore = 1;
            @throw.PlayerId = player.PlayerId;

            // Act
            var response = await throwsController.Edit(1, @throw.Leg.Set.GameId, @throw);

            // Assert
            var redirectResult = response as RedirectToActionResult;
            Assert.IsNotNull(redirectResult);

            var storedThrow = await context.Throws.FirstAsync(t => t.ThrowId == 1);
            Assert.AreEqual(@throw.ThrowId, storedThrow.ThrowId);
            Assert.AreEqual(@throw.PlayerId, storedThrow.PlayerId);
            Assert.AreEqual(@throw.FirstScore, storedThrow.FirstScore);
            Assert.AreEqual(@throw.SecondScore, storedThrow.SecondScore);
            Assert.AreEqual(@throw.ThirdScore, storedThrow.ThirdScore);
        }
        #endregion

        #region Delete
        [TestMethod]
        public async Task ThrowDeleteShouldReturn404WhenNoIdIsProvided()
        {
            // Act
            var result = await throwsController.Delete(null);

            // Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task ThrowDeleteShouldReturn404WhenThrowNotFound()
        {
            // Act
            var result = await throwsController.Delete(3);

            // Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task ThrowDeleteShouldReturnConfirmPageWhenFound()
        {
            // Arrange
            var @throw = await context.Throws.FirstAsync(g => g.ThrowId == 1);

            // Act
            var result = await throwsController.Delete(1);

            // Assert
            var view = result as ViewResult;
            Assert.IsNotNull(view);
            var model = view.Model as Throw;
            Assert.IsNotNull(model);
            Assert.AreEqual(@throw.ThrowId, model.ThrowId);
        }

        [TestMethod]
        public void ThrowDeleteConfirmedShouldThrowWhenThrowNotFound()
        {
            // Act => Assert
            Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await throwsController.DeleteConfirmed(2));
        }

        [TestMethod]
        public async Task ThrowDeleteConfirmShouldDeleteWhenThrowIsFoundAndRedirect()
        {
            // Act
            var result = await throwsController.DeleteConfirmed(1);

            // Assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            var isThrowDelete = context.Throws.Any(g => g.ThrowId == 1) == false;
            Assert.IsTrue(isThrowDelete);
        }
        #endregion
    }
}
